# Plugin WooCommerce Comisiones Vendedor

Plugin para generar una URL dinámica por cada usuario con rol DISTRIBUIDOS. Permite manejar comisiones para cada vendedor en WooCommerce. Repositorio privado https://gitlab.com/diurvan/woocommerce-comisiones-vendedor